#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import numpy

import read_radolan.Metadata


class RadolanData:
    """
    RadolanData class contains all relevant data of radolan formatted data.
    """
    def __init__(self,
                 data: numpy.ndarray = None,
                 metadata: read_radolan.Metadata.Metadata = None,
                 idx_clutter: numpy.ndarray = None,
                 idx_nan: numpy.ndarray = None):
        """
        Initialize RadolanData class.

        :param data: 2D matrix with radolan data
        :type data: numpy.ndarray, optional
        :param metadata: describing metadata from radolan data
        :type metadata: read_radolan.Metadata.Metadata, optional
        :param idx_clutter: 2D matrix with True/1 for clutter in radolan data und False/0 otherwise
        :type idx_clutter: numpy.ndarray, optional
        :param idx_nan: 2D matrix with True/1 for nan in radolan data and False/0 otherwise
        :type idx_nan: numpy.ndarray, optional
        """
        self.data = data
        self.metadata = metadata
        self.idx_clutter = idx_clutter
        self.idx_nan = idx_nan
