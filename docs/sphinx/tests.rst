tests
=====

For classes and functions unittests are available to ensure the code working as intended.

test\_read\_radolan module
--------------------------

.. automodule:: tests.test_read_radolan
   :members:
   :undoc-members:
   :show-inheritance:

test\_read\_icon module
-----------------------

.. automodule:: tests.test_read_icon
   :members:
   :undoc-members:
   :show-inheritance:

test\_read\_cosmo module
------------------------

.. automodule:: tests.test_read_cosmo
   :members:
   :undoc-members:
   :show-inheritance:

test\_met\_entities module
--------------------------

.. automodule:: tests.test_met_entities
   :members:
   :undoc-members:
   :show-inheritance:

test\_aux\_tools module
-----------------------

.. automodule:: tests.test_aux_tools
   :members:
   :undoc-members:
   :show-inheritance:

test\_download\_data module
---------------------------

.. automodule:: tests.test_download_data
   :members:
   :undoc-members:
   :show-inheritance:
