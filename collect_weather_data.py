#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de

import json
import sys
import numpy as np
import datetime as dt
from isodate import parse_duration

from met_entities.VariableDescription import RegridDescription, DataDescription
from met_entities.LonLatTime import LonLatTime
from read_radolan.readRadolan import get_lonlat
from met_entities.WeatherData import WeatherData


def main(filename_config):
    """
    Read in a configuration file (json format) with the description of requested meteo data to load. It uses general
    parameters like the time or the target grid, a data collection with the precipitation products in the order of
    loading, and specifies the export to netcdf. Internally the WeatherData class is used to collect the data.

    This module provides a possibility to get harmonized precipitation data via config file without writing code. The
    module can be invoked from outside and results in a netcdf file.

    :param filename_config: name of config file with an appropriate json structure
    :type filename_config: str
    :return: return code 0 means all data was successfully loaded, return code 8 means some data product could not be
        loaded and not netcdf file was written
    :rtype: int
    """
    with open(filename_config, mode='r') as fid:
        # read config file content
        config = json.loads(fid.read())

    # target grid definition
    if config['target_grid'].lower() == 'saxony_clip_de1200':
        lon_target, lat_target = get_lonlat(5, 'de1200')
        idx = np.all(
            (lon_target.data >= 11.7, lon_target.data <= 15.2, lat_target.data >= 50.1, lat_target.data <= 51.8),
            axis=0)
        idx_lon = np.any(idx, axis=0).nonzero()[0]
        idx_lat = np.any(idx, axis=1).nonzero()[0]
        lon_target.data = lon_target.data[idx_lat[0]:idx_lat[-1], idx_lon[0]:idx_lon[-1]]
        lat_target.data = lat_target.data[idx_lat[0]:idx_lat[-1], idx_lon[0]:idx_lon[-1]]
    elif config['target_grid'].lower() == 'oman_clip':
        lon = np.arange(55.8, 57.6, .02)
        lat = np.arange(24.6, 23, -.02)
        lon_target, lat_target = np.meshgrid(lon, lat)
        lon_description = DataDescription(coordinate_system='WGS 84, EPSG:4326', long_name='longitude of center',
                                          units='degrees_east')
        lat_description = DataDescription(coordinate_system='WGS 84, EPSG:4326', long_name='latitude of center',
                                          units='degrees_north')
        lon_target = LonLatTime(data=lon_target, data_description=lon_description)
        lat_target = LonLatTime(data=lat_target, data_description=lat_description)
    else:
        raise Exception('currently supported target grids:\n'
                        'saxony_clip_de1200\n'
                        f'target_grid {config["target_grid"]} not supported')

    # regridding definition
    regrid_description = {}
    for data_collection in config['data_collection']:
        if 'neighbors' in data_collection:
            neighbors = data_collection['neighbors']
        else:
            neighbors = 3
        if 'regrid_description_file' in data_collection:
            regrid_description_file = data_collection['regrid_description_file']
        else:
            regrid_description_file = None
        regrid_description[data_collection['product']] = \
            RegridDescription(lon_target=lon_target, lat_target=lat_target, neighbors=neighbors,
                              file_nearest=regrid_description_file)

    # general weatherdata definitions
    if config['time_now'] == 'now':
        time_now = dt.datetime.now(tz=dt.timezone.utc)
    else:
        time_now = dt.datetime.fromisoformat(config['time_now'])
    delta_t = parse_duration(config['dt'])

    wd = WeatherData(time_now=time_now, delta_t=delta_t, fill_value=config['fill_value'],
                     scale_factor=config['scale_factor'], regrid_description=regrid_description,
                     short=config['short'])

    # collect all requested meteorological data
    for ct, data_collection in enumerate(config['data_collection']):
        if ct == 0 and data_collection['product'] not in ['radolanrw', 'pyradman', 'zeros']:
            raise Exception('product radolanrw or pyradman or zeros must be the first in the data collection')

        if 'dir_time_descriptor' in data_collection:
            dir_time_descriptor = data_collection['dir_time_descriptor']
        else:
            dir_time_descriptor = None

        if data_collection['product'].lower() == 'radolanrw':
            # specific handling of radolanrw data
            time_radrw_start = time_now + parse_duration(data_collection['time_start'])
            time_radrw_start = dt.datetime(time_radrw_start.year, time_radrw_start.month, time_radrw_start.day,
                                           time_radrw_start.hour, tzinfo=time_radrw_start.tzinfo)
            if 'radolanrw_nc_filename' in data_collection:
                radolanrw_nc_filename = data_collection['radolanrw_nc_filename']
            else:
                radolanrw_nc_filename = None

            wd.collect_radolanrw(time_start=time_radrw_start, directory=data_collection['directory'],
                                 dir_time_descriptor=dir_time_descriptor,
                                 radolanrw_nc_filename=radolanrw_nc_filename)

        elif data_collection['product'].lower() == 'pyradman':
            # specific handling of pyradman data
            time_pyradman_start = time_now + parse_duration(data_collection['time_start'])
            time_pyradman_start = dt.datetime(time_pyradman_start.year, time_pyradman_start.month, time_pyradman_start.day,
                                              time_pyradman_start.hour, tzinfo=time_pyradman_start.tzinfo)
            if 'pyradman_nc_filename' in data_collection:
                pyradman_nc_filename = data_collection['pyradman_nc_filename']
            else:
                pyradman_nc_filename = None
            if 'variant' in data_collection:
                variant = data_collection['variant']
            else:
                raise Exception('variant must be designated for pyradman data ("AC", "AL", or "AS")')

            wd.collect_pyradman(time_start=time_pyradman_start, directory=data_collection['directory'],
                                dir_time_descriptor=dir_time_descriptor,
                                pyradman_nc_filename=pyradman_nc_filename, variant=variant)

        elif data_collection['product'].lower() == 'zeros':
            # specific handling of zeros observation data
            time_zeros_start = time_now + parse_duration(data_collection['time_start'])
            time_zeros_start = dt.datetime(time_zeros_start.year, time_zeros_start.month,
                                           time_zeros_start.day,
                                           time_zeros_start.hour, tzinfo=time_zeros_start.tzinfo)
            wd.collect_zeros(time_start=time_zeros_start)

        else:
            # all supported forecast products
            if 'latest_event' in data_collection:
                if data_collection['latest_event'] == 'now':
                    latest_event = time_now
                else:
                    latest_event = dt.datetime.fromisoformat(data_collection['latest_event'])
            else:
                latest_event = time_now

            rc = 0
            if data_collection['product'].lower() == 'iconeps':
                rc = wd.collect_iconeps(latest_event=latest_event, directory=data_collection['directory'],
                                        dir_time_descriptor=dir_time_descriptor,
                                        forecast_hours=data_collection['forecast_hours'])
            elif data_collection['product'].lower() == 'icon':
                rc = wd.collect_icon(latest_event=latest_event, directory=data_collection['directory'],
                                     dir_time_descriptor=dir_time_descriptor,
                                     forecast_hours=data_collection['forecast_hours'])
            elif data_collection['product'].lower() == 'iconeueps':
                rc = wd.collect_iconeueps(latest_event=latest_event, directory=data_collection['directory'],
                                          dir_time_descriptor=dir_time_descriptor,
                                          forecast_hours=data_collection['forecast_hours'])
            elif data_collection['product'].lower() == 'iconeu':
                rc = wd.collect_iconeu(latest_event=latest_event, directory=data_collection['directory'],
                                       dir_time_descriptor=dir_time_descriptor,
                                       forecast_hours=data_collection['forecast_hours'])
            elif data_collection['product'].lower() == 'icond2eps':
                rc = wd.collect_icond2eps(latest_event=latest_event, directory=data_collection['directory'],
                                          dir_time_descriptor=dir_time_descriptor,
                                          forecast_hours=data_collection['forecast_hours'])
            elif data_collection['product'].lower() == 'icond2':
                rc = wd.collect_icond2(latest_event=latest_event, directory=data_collection['directory'],
                                       dir_time_descriptor=dir_time_descriptor,
                                       forecast_hours=data_collection['forecast_hours'])
            elif data_collection['product'].lower() == 'radvorrq':
                rc = wd.collect_radvorrq(latest_event=latest_event, directory=data_collection['directory'],
                                         dir_time_descriptor=dir_time_descriptor)
            elif data_collection['product'].lower() == 'radolanrv':
                rc = wd.collect_radolanrv(latest_event=latest_event, directory=data_collection['directory'],
                                          dir_time_descriptor=dir_time_descriptor)
            elif data_collection['product'].lower() == 'intense':
                rc = wd.collect_intense(latest_event=latest_event, directory=data_collection['directory'],
                                        dir_time_descriptor=dir_time_descriptor)

            if rc != 0:
                return 8

    filename = config['export_netcdf']['filename']
    if 'institution' in config['export_netcdf']:
        institution = config['export_netcdf']['institution']
    else:
        institution = None
    if 'data_format' in config['export_netcdf']:
        data_format = config['export_netcdf']['data_format']
    else:
        data_format = None
    if 'scale_factor_nc' in config['export_netcdf']:
        scale_factor_nc = config['export_netcdf']['scale_factor_nc']
    else:
        scale_factor_nc = None
    if 'scale_undo' in config['export_netcdf']:
        scale_undo = config['export_netcdf']['scale_undo']
    else:
        scale_undo = None
    if 'data_kwargs' in config['export_netcdf']:
        data_kwargs = config['export_netcdf']['data_kwargs']
    else:
        data_kwargs = None

    mode = 'create'
    if 'mode' in config['export_netcdf']:
        mode = config['export_netcdf']['mode']
    max_forecast_length = None
    if 'max_forecast_length' in config['export_netcdf']:
        max_forecast_length = parse_duration(config['export_netcdf']['max_forecast_length'])
    if mode.lower() == 'create':
        # create new file
            wd.export_netcdf(filename=filename, institution=institution, data_format=data_format,
                             scale_factor_nc=scale_factor_nc, scale_undo=scale_undo, data_kwargs=data_kwargs,
                             max_forecast_length=max_forecast_length)
    else:
        # append to existing file
        wd.export_netcdf_append(filename=filename, max_forecast_length=max_forecast_length)

    return 0

if __name__ == '__main__':
    sys.exit(main(filename_config=sys.argv[1]))
