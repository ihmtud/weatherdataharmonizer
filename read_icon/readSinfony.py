#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import importlib.resources
import os
import datetime as dt
import copy
import numpy as np
import pygrib

from met_entities.LonLatTime import LonLatTime
from met_entities.VariableDescription import DataDescription
from read_icon.IconData import IconData
from read_icon.Metadata import Metadata
from aux_tools import grib2_tools as gt


def read_intense(filename: str, scale_factor: float = 1, fill_value: float = -999, short: str = None):
    """
    Read in Sinfony-INTENSE data from file, either as grib2 file or as bz2 compressed grib2 file.

    :param filename: name of file
    :type filename: str
    :param scale_factor: the final data has to be multiplied with this value
    :type scale_factor: float, optional
    :param fill_value: missing data is filled with that value
    :type fill_value: float, optional
    :param short: if int16 or int32, the large data variables are cast to numpy.int16/int32 to minimize memory
        usage; the user must pay attention to the scale_factor to include the necessary precision
    :type short: str, optional
    :return: Sinfony INTENSE data of four 15 min values (for some older data also one 1 h value possible) as in the
        content of the grib2 file
    :rtype: list
    """
    if short and scale_factor >= 1:
        print(f'the attribute short casts the data to 16 bit signed integer for memory saving; the scale_factor = '
              f'{scale_factor} imposes a low precision')
    if short and np.isnan(fill_value):
        raise Exception(f'if short data type is requested, the fill_value must not be nan')
    if short and (short != 'int16' and short != 'int32'):
        raise Exception(f'datatype {short} not supported; please change to int16 or int32')

    # read data from icon grib2 file
    intense_data = []  # list of IconData() objects for INTENSE data

    idx_bz2 = False
    if filename[-4:] == '.bz2':
        # file is bz2 compressed
        idx_bz2 = True
        filename = gt.write_temporary_grib_file(filename)

    grbs = pygrib.open(filename)
    for grb in grbs:
        # get metadata
        metadata = Metadata()
        metadata.centre = grb['centre']
        metadata.centre_description = grb['centreDescription']
        metadata.datum = dt.datetime(grb['year'], grb['month'], grb['day'], grb['hour'], grb['minute'])
        if 'yearOfEndOfOverallTimeInterval' in grb.keys():
            # 15 min products do not contain useful content in grib message key 'forecastTime' -> build information
            # from 'yearOfEndOfOverallTimeInterval' and similar
            metadata.datum_end_of_overall_time_interval = dt.datetime(grb['yearOfEndOfOverallTimeInterval'],
                                                                      grb['monthOfEndOfOverallTimeInterval'],
                                                                      grb['dayOfEndOfOverallTimeInterval'],
                                                                      grb['hourOfEndOfOverallTimeInterval'],
                                                                      grb['minuteOfEndOfOverallTimeInterval'])
        else:
            # currently only hourly products contain useful content in grib message key 'forecastTime'
            metadata.datum_end_of_overall_time_interval = metadata.datum + dt.timedelta(minutes=grb['forecastTime'])
        metadata.datum_iso = metadata.datum_end_of_overall_time_interval.replace(tzinfo=dt.timezone.utc).isoformat()
        metadata.prediction_time = metadata.datum_end_of_overall_time_interval - metadata.datum
        metadata.step_type = grb['stepType']
        metadata.grid_type = grb['gridType']
        metadata.name = grb['name']
        metadata.units = grb['units']
        metadata.missing_value = grb['missingValue']
        metadata.number_of_missing = grb['numberOfMissing']
        metadata.number_of_data_points = grb['numberOfDataPoints']
        metadata.fill_value = fill_value

        # get data and replace missing values with fill_value
        data = np.flipud(grb.values.data)  # flip upside down to make correct coordinates
        idx_missing = np.where(data == metadata.missing_value)
        data = data / scale_factor
        data[idx_missing] = fill_value

        if short == 'int16':
            intense_data.append(IconData(data=np.short(np.around(data)), metadata=metadata))
        elif short == 'int32':
            intense_data.append(IconData(data=np.int32(np.around(data)), metadata=metadata))
        else:
            intense_data.append(IconData(data, metadata=metadata))

    grbs.close()

    if idx_bz2:
        os.remove(filename)

    return intense_data


def read_ruc(filename: str, variable: str = 'TOT_PREC', scale_factor: float = 1, fill_value: float = -999,
             short: str = None):
    """
    Read in Sinfony-RUC/RUC-EPS data from file, either as grib2 file or as bz2 compressed grib2 file.

    :param filename: name of file
    :type filename: str
    :param variable: DWD nomenclature variable name of variable to be read; possible choices for RUC are:
        ASWDIFD_S - diffuse downward short-wave radiation at surface
        ASWDIR_S - direct short-wave radiation at surface
        GRAU_GSP - accumulated graupel (also in RUC-EPS)
        RAIN_GSP - accumulated fluid precipitation (also in RUC-EPS)
        SNOW_GSP - accumulated snow precipitation (also in RUC-EPS)
        TOT_PREC - accumulated total precipitation (also in RUC-EPS)
        PMSL - reduced surface air pressure
        TD_2M - dewpoint temperature at 2 m
        T_2M - air temperature at 2 m
        U_10M - zonal wind speed at 10 m
        V_10M - meridional wind speed at 10 m
    :param scale_factor: the final data has to be multiplied with this value
    :type scale_factor: float, optional
    :param fill_value: missing data is filled with that value
    :type fill_value: float, optional
    :param short: if int16 or int32, the large data variables are cast to numpy.int16/int32 to minimize memory
        usage; the user must pay attention to the scale_factor to include the necessary precision
    :type short: str, optional
    :return: Sinfony-RUC data of four 15 min values (for some older data also one 1 h value possible) as in the
        content of the grib2 file
    :rtype: list
    """
    if short and scale_factor >= 1:
        print(f'the attribute short casts the data to 16 bit signed integer for memory saving; the scale_factor = '
              f'{scale_factor} imposes a low precision')
    if short and np.isnan(fill_value):
        raise Exception(f'if short data type is requested, the fill_value must not be nan')
    if short and (short != 'int16' and short != 'int32'):
        raise Exception(f'datatype {short} not supported; please change to int16 or int32')

    # dictionary of DWD vs. grib shortName keys for variables
    name_dict = {'ASWDIFD_D': 'ASWDIFD_S',
                 'ASWDIR_S': 'ASWDIR_S',
                 'GRAU_GSP': 'GRAU_GSP',
                 'PMSL': 'prmsl',
                 'RAIN_GSP': 'RAIN_GSP',
                 'SNOW_GSP': 'SNOW_GSP',
                 'TD_2M': '2d',
                 'TOT_PREC': 'tp',
                 'T_2M': '2t',
                 'U_10M': '10u',
                 'V_10M': '10v'}

    # read data from ruc grib2 file
    ruc_data = []  # list of IconData() objects for RUC data

    idx_bz2 = False
    if filename[-4:] == '.bz2':
        # file is bz2 compressed
        idx_bz2 = True
        filename = gt.write_temporary_grib_file(filename)

    grbs = pygrib.open(filename)

    variable_names = [grbs.message(i)['shortName'] for i in range(1, grbs.messages + 1)]
    variable_idx = [i + 1 for i, val in enumerate(variable_names) if val == name_dict[variable]]

    for message in variable_idx:
        grb = grbs.message(message)

        # get metadata
        metadata = Metadata()
        metadata.centre = grb['centre']
        metadata.centre_description = grb['centreDescription']
        metadata.datum = dt.datetime(grb['year'], grb['month'], grb['day'], grb['hour'], grb['minute'])
        if 'yearOfEndOfOverallTimeInterval' in grb.keys():
            # 15 min products do not contain useful content in grib message key 'forecastTime' -> build information
            # from 'yearOfEndOfOverallTimeInterval' and similar
            metadata.datum_end_of_overall_time_interval = dt.datetime(grb['yearOfEndOfOverallTimeInterval'],
                                                                      grb['monthOfEndOfOverallTimeInterval'],
                                                                      grb['dayOfEndOfOverallTimeInterval'],
                                                                      grb['hourOfEndOfOverallTimeInterval'],
                                                                      grb['minuteOfEndOfOverallTimeInterval'])
        else:
            # currently only hourly products contain useful content in grib message key 'forecastTime'
            metadata.datum_end_of_overall_time_interval = metadata.datum + dt.timedelta(minutes=grb['forecastTime'])
        metadata.datum_iso = metadata.datum_end_of_overall_time_interval.replace(tzinfo=dt.timezone.utc).isoformat()
        metadata.prediction_time = metadata.datum_end_of_overall_time_interval - metadata.datum
        metadata.step_type = grb['stepType']
        metadata.grid_type = grb['gridType']
        metadata.name = grb['name']
        metadata.units = grb['units']
        metadata.missing_value = grb['missingValue']
        metadata.number_of_missing = grb['numberOfMissing']
        metadata.number_of_data_points = grb['numberOfDataPoints']
        metadata.fill_value = fill_value

        # get data and replace missing values with fill_value
        data = np.flipud(grb.values.data)  # flip upside down to make correct coordinates
        idx_missing = np.where(data == metadata.missing_value)
        data = data / scale_factor
        data[idx_missing] = fill_value

        if short == 'int16':
            ruc_data.append(IconData(data=np.short(np.around(data)), metadata=metadata))
        elif short == 'int32':
            ruc_data.append(IconData(data=np.int32(np.around(data)), metadata=metadata))
        else:
            ruc_data.append(IconData(data, metadata=metadata))

    # temporal resolution must be constant and equal to the first step
    delta_t = ruc_data[1].metadata.prediction_time - ruc_data[0].metadata.prediction_time
    delta_t_correct = [ruc_data[i + 1].metadata.prediction_time - ruc_data[i].metadata.prediction_time == delta_t
                       for i in range(len(ruc_data) - 1)]
    if False in delta_t_correct:
        idx = delta_t_correct.index(False) + 1
        print(f'forecast contains changing delta t: delete forecasts from prediction time '
              f'{ruc_data[idx].metadata.prediction_time} on')
        ruc_data = ruc_data[:idx]

    # if some forecast steps are missing, fill until 12 h forecast
    if ruc_data[-1].metadata.prediction_time < dt.timedelta(hours=12):
        print(f'forecast only up to {ruc_data[-1].metadata.prediction_time} - extend to 12 h')

        while ruc_data[-1].metadata.prediction_time < dt.timedelta(hours=12):
            ruc_data.append(copy.deepcopy(ruc_data[-1]))
            ruc_data[-1].data[:] = fill_value
            ruc_data[-1].metadata.datum_end_of_overall_time_interval = (
                    ruc_data[-2].metadata.datum_end_of_overall_time_interval + delta_t)
            ruc_data[-1].metadata.datum_iso = (
                ruc_data[-1].metadata.datum_end_of_overall_time_interval.replace(tzinfo=dt.timezone.utc).isoformat())
            ruc_data[-1].metadata.number_of_missing = ruc_data[-1].data.size
            ruc_data[-1].metadata.prediction_time = ruc_data[-1].metadata.datum_end_of_overall_time_interval - \
                ruc_data[-1].metadata.datum

    grbs.close()

    if idx_bz2:
        os.remove(filename)

    return ruc_data


def get_lonlat(inv_file: str = None):
    """
    Read longitudes, latitudes, and altitude from inv file (either grib2 files or bz2 compressed files).

    :param inv_file: grib2 or bz2 compressed grib2 file with center altitudes, longitudes, latitudes of INTENSE; if not
        given the file from the resources directory in this package is used
    :type inv_file: str, optional
    :return: center altitudes, longitudes, and latitudes
    :rtype: met_entities.LonLatTime.LonLatTime, met_entities.LonLatTime.LonLatTime
    """
    if inv_file is None:
        # import files from sub folder resources
        inv_file = 'resources/sinfony_ruc_inv.grib2.bz2'
        import read_icon
        resources_dir = str(importlib.resources.files(read_icon))
        inv_file_complete = os.path.join(resources_dir, inv_file)
    else:
        # inv file is given externally
        inv_file_complete = inv_file

    idx_bz2_inv = False
    if inv_file_complete[-4:] == '.bz2':
        # the given inv file is bz2 compressed
        idx_bz2_inv = True
        inv_file_complete = gt.write_temporary_grib_file(inv_file_complete)

    grbs = pygrib.open(inv_file_complete)

    # the altitude is read but currently not used to be in line with readIcon.get_lonlat function
    calt_data = np.flipud(grbs.message(1).values.data)  # flip upside down
    calt_missing_value = grbs.message(1)['missingValue']
    idx_missing = np.where(calt_data == calt_missing_value)
    calt_data[idx_missing] = np.nan
    calt_description = DataDescription(units=grbs.message(1)['units'], long_name=grbs.message(1)['parameterName'],
                                       standard_name=grbs.message(1)['shortNameECMF'])
    calt = LonLatTime(data=calt_data, data_description=calt_description)

    clon_data = np.flipud(grbs.message(2).values.data)  # flip upside down
    clon_missing_value = grbs.message(2)['missingValue']
    idx_missing = np.where(clon_data == clon_missing_value)
    clon_data[idx_missing] = np.nan
    # some cells do not have latitude values -> fill regarding the orthogonal grid
    # fill first line
    idx = np.where(np.isnan(clon_data[0, :]))[0]
    diff = clon_data[0, 1] - clon_data[0, 0]
    for idx_act in range(idx[0], idx[-1] + 1):
        clon_data[0, idx_act] = clon_data[0, idx_act - 1] + diff
    # fill all other lines column wise
    for ct in range(clon_data.shape[1]):
        idx = np.isnan(clon_data[:, ct])
        clon_data[idx, ct] = clon_data[0, ct]
    clon_description = DataDescription(units=grbs.message(2)['units'], long_name=grbs.message(2)['parameterName'],
                                       standard_name=grbs.message(2)['shortNameECMF'],
                                       coordinate_system='WGS 84, EPSG:4326')
    clon = LonLatTime(data=clon_data, data_description=clon_description)

    clat_data = np.flipud(grbs.message(3).values.data)  # flip upside down
    clat_missing_value = grbs.message(3)['missingValue']
    idx_missing = np.where(clat_data == clat_missing_value)
    clat_data[idx_missing] = np.nan
    for ct in range(clat_data.shape[0]):
        # some cells do not have latitude values -> fill regarding the orthogonal grid
        idx = np.isnan(clat_data[ct, :])
        clat_data[ct, idx] = clat_data[ct, 0]
    clat_description = DataDescription(units=grbs.message(3)['units'], long_name=grbs.message(3)['parameterName'],
                                       standard_name=grbs.message(3)['shortNameECMF'],
                                       coordinate_system='WGS 84, EPSG:4326')
    clat = LonLatTime(data=clat_data, data_description=clat_description)

    grbs.close()

    if idx_bz2_inv:
        os.remove(inv_file_complete)

    return clon, clat
