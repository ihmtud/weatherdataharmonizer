#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import numpy

import read_icon.Metadata


class IconData:
    """
    IconData class contains all relevant data and metadata of Icon formatted data.
    """
    def __init__(self,
                 data: numpy.ndarray = None,
                 metadata: read_icon.Metadata.Metadata = None):
        """
        Initialize IconData class.

        :param data: 1D array of data
        :type data: numpy.ndarray, optional
        :param metadata: describing metadata from icon data
        :type metadata: read_icon.Metadata.Metadata, optional
        """
        self.data = data
        self.metadata = metadata
