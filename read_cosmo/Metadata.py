#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import datetime


class Metadata:
    """
    Metadata class contains relevant metadata of cosmo formatted data.
    """
    def __init__(self,
                 centre: str = None,
                 centre_description: str = None,
                 datum: datetime.datetime = None,
                 datum_end_of_overall_time_interval: datetime.datetime = None,
                 datum_iso: str = None,
                 prediction_time: datetime.timedelta = None,
                 step_type: str = None,
                 grid_type: str = None,
                 name: str = None,
                 units: str = None,
                 missing_value: float = None,
                 number_of_missing: int = None,
                 number_of_data_points: int = None,
                 fill_value: float = None):
        """
        Initialize Metadata class.

        :param centre: centre responsible for data creation
        :type centre: str, optional
        :param centre_description: further description of the centre
        :type centre_description: str, optional
        :param datum: UTC datum/time of simulation start
        :type datum: datetime.datetime, optional
        :param datum_end_of_overall_time_interval: UTC datum/time of current simulation end
        :type datum_end_of_overall_time_interval: datetime.datetime, optional
        :param datum_iso: ISO 8601 compliant datetime of current simulation end
        :type datum_iso: str, optional
        :param prediction_time: timedelta from datum to datum_end_of_overall_time_interval
        :type prediction_time: datetime.timedelta, optional
        :param step_type: type of step, e.g. 'accum' for accumulated data
        :type step_type: str, optional
        :param grid_type: type of grid, e.g. 'unstructured grid' for non-orthogonal grids
        :type grid_type: str, optional
        :param name: name of data
        :type name: str, optional
        :param units: unit of data
        :type units: str, optional
        :param missing_value: original missing value indicator in grib file
        :type missing_value: float, optional
        :param number_of_missing: number of missing values
        :type number_of_missing: int, optional
        :param number_of_data_points: total number of data points
        :type number_of_data_points: int, optional
        :param fill_value: missing value indicator in corresponding read_cosmo.CosmoData.CosmoData object
        :type fill_value: float, optional
        """
        self.centre = centre
        self.centre_description = centre_description
        self.datum = datum
        self.datum_end_of_overall_time_interval = datum_end_of_overall_time_interval
        self.datum_iso = datum_iso
        self.prediction_time = prediction_time
        self.step_type = step_type
        self.grid_type = grid_type
        self.name = name
        self.units = units
        self.missing_value = missing_value
        self.number_of_missing = number_of_missing
        self.number_of_data_points = number_of_data_points
        self.fill_value = fill_value
