#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import numpy

import read_cosmo.Metadata


class CosmoData:
    """
    CosmoData class contains all relevant data and metadata of Cosmo formatted data.
    """
    def __init__(self,
                 data: numpy.ndarray = None,
                 metadata: read_cosmo.Metadata.Metadata = None):
        """
        Initialize CosmoData class.

        :param data: 1D array of data
        :type data: numpy.ndarray, optional
        :param metadata: describing metadata from icon data
        :type metadata: read_cosmo.Metadata.Metadata, optional
        """
        self.data = data
        self.metadata = metadata
