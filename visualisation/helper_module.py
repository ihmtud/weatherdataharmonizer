#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import numpy as np


def data_source_names(data_source_id: int):
    data_source_dict = {0: 'RadolanRW',
                        1: 'RadvorRQ',
                        2: 'RadolanRV',
                        3: 'IconD2',
                        4: 'IconD2EPS',
                        5: 'IconEU',
                        6: 'IconEUEPS',
                        7: 'Icon',
                        8: 'Intense',
                        9: 'RUC'}
    return data_source_dict[data_source_id]

def cal_figsize(lon: np.ndarray, lat: np.ndarray, figure_pixel_height: int = 1200, dpi: int = 300):
    """
    Calculate a suitable figure size for a particular lon/lat region. With the equations:
    latitude: 1 deg = 110.574 km
    longitude: 1 deg = 111.32 * cos(latitude) km
    the figure dimension sizes are calculated with the best possible kilometer-aware isotropy. The space for a colorbar
    is not regarded as well as a potentially exported tight boundary box.

    :param lon: matrix of longitudes; only the min, max are taken
    :type lon: np.ndarray
    :param lat: matrix of latitudes; only the min, max, mean are taken
    :type lat: np.ndarray
    :param figure_pixel_height: number of pixels the figure shall have in height; defaults to 1200
    :type figure_pixel_height: int
    :param dpi: dpi count for figure; defaults to 300
    :type dpi: int, optional
    :return: width and height recommendation for figure
    :rtype: tuple
    """
    dist_lat = 110.574
    min_lat = np.min(lat)
    max_lat = np.max(lat)
    dist_lat_range = (max_lat - min_lat) * dist_lat

    dist_lon = 111.32 * np.cos(np.deg2rad(np.mean(lat)))
    min_lon = np.min(lon)
    max_lon = np.max(lon)
    dist_lon_range = (max_lon - min_lon) * dist_lon

    height = figure_pixel_height / dpi
    width = figure_pixel_height / dist_lat_range * dist_lon_range / dpi

    return width, height
