#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
import matplotlib.pyplot as plt
from matplotlib.animation import FuncAnimation
import netCDF4 as nc
import datetime as dt
import numpy as np
import os

from visualisation.helper_module import *


class WeatherData:
    """
    WeatherData class provides a plotting functionality for met_entities.WeatherData.WeatherData classes.
    """
    def __init__(self,
                 filename: str):
        """
        Initialize WeatherData class.

        :param filename: filename of netcdf file formerly generated with met_entities.WeatherData.WeatherData
        :type: str
        """
        self.filename = os.path.abspath(filename)
        self.filename_base = os.path.basename(self.filename)

        if self.filename_base[-3:] != '.nc':
            raise Exception(f'{self.filename_base} does not end with ".nc"')

        # load data
        # TODO: support multiple ensemble members
        with nc.Dataset(self.filename, mode='r') as nc_file:
            nc_vars = nc_file.variables
            self.lon = nc_vars['longitude'][:].data
            self.lat = nc_vars['latitude'][:].data
            self.precip = nc_vars['precipitation'][:].data
            fill_value = nc_vars['precipitation']._FillValue
            self.precip[self.precip == fill_value] = np.nan
            time = nc_vars['time'][:].data
            time_units = nc_vars['time'].units.split(' ')
            self.time_values = [dt.datetime.fromisoformat(time_units[-1]) + dt.timedelta(hours=time[i]) for i in
                                range(len(time))]
            self.data_source = nc_vars['data_source'][:].data

        # find last observation date
        idx_last_obs = np.where(self.data_source == 0)[0][-1]
        self.time_last_obs = self.time_values[idx_last_obs]

        self.contour_handle = None
        self.color_arr = None
        self.levels = None
        self.datum_handle = None
        self.product_handle = None

    def plot_single(self, dir_out: str, time_idx: list = None):
        """
        Produce single plots of this WeatherData object. Via the time_idx a list of time step indexes can be given to
        specify the required plots.

        :param dir_out: directory for output file; the filename of the written png file mirrors the basename of the
            netcdf file extended with "_xxx.png" (index number, starting with 0)
        :type dir_out: str
        :param time_idx: list of time step indexes at which a plot is built; if not given, only the first time step is
            plotted
        :type time_idx: list, optional
        """
        if time_idx is None:
            time_idx = [0]

        # plot
        fig_width, fig_height = cal_figsize(lon=self.lon, lat=self.lat, figure_pixel_height=1200, dpi=300)
        plt.figure(1, figsize=(fig_width * 1.25, fig_height), dpi=300)  # * 1.25 for colorbar
        for ct in time_idx:
            if ct == 0:
                self.plotting_first(time_idx=ct)
            else:
                self.plotting_seconds(time_idx=ct)

            # construct filename and write png
            filenameout = self.filename_base[:-3]
            filenameout = os.path.join(dir_out, f'{filenameout}_{ct:03d}.png')
            plt.savefig(filenameout, bbox_inches='tight')

    def animate_plot(self, dir_out: str, time_idx: list = None):
        """
        Produce an animation of this WeatherData object. Via the time_idx a list of time step indexes can be given to
        specify the required plots.

        :param dir_out: directory for output file; the filename of the written mp4 file mirrors the basename of the
            netcdf file extended with "_animation.mp4"
        :type dir_out: str
        :param time_idx: list of time step indexes at which a plot is built; if not given, only the first time step is
            plotted
        :type time_idx: list, optional
        """
        if time_idx is None:
            time_idx = [0]

        # plot
        fig_width, fig_height = cal_figsize(lon=self.lon, lat=self.lat, figure_pixel_height=1200, dpi=300)
        fig = plt.figure(1, figsize=(fig_width * 1.25, fig_height), dpi=300)  # * 1.25 for colorbar

        animation = FuncAnimation(fig=fig, init_func=self.plotting_first(time_idx=time_idx[0]),
                                  func=self.plotting_seconds, frames=time_idx[1:], interval=50)
        filenameout = self.filename_base[:-3]
        filenameout = os.path.join(dir_out, f'{filenameout}_animation.mp4')
        animation.save(filename=filenameout, writer='ffmpeg')

    def plotting_first(self, time_idx: int):
        """
        Make the first contour plot of x, y, z variables with the color standards by the German Weather Service (DWD).

        :param time_idx: number of time step index at which the plot is built
        :type time_idx: int
        """
        print(f'time step {time_idx:03d}')

        # color definitions following DWD standards
        self.levels = [0, .1, .2, .5, 1, 2, 5, 10, 15, 25, 40, 60, 80, 100]
        rgb_colors = [[90, 90, 90],
                      [99, 100, 76],
                      [98, 100, 36],
                      [88, 99, 15],
                      [63, 84, 15],
                      [27, 77, 48],
                      [0, 84, 85],
                      [7, 63, 84],
                      [3, 1, 99],
                      [57, 20, 72],
                      [86, 16, 78],
                      [91, 5, 5],
                      [53, 6, 5],
                      [31, 6, 5]]
        self.color_arr = []
        for color in rgb_colors:
            rgb = [float(value) / 100 for value in color]
            self.color_arr.append(rgb)

        # plotting
        plt.clf()
        ax = plt.gca()
        if self.lon.ndim == 2:
            # 2D data
            self.contour_handle = ax.contourf(self.lon, self.lat, self.precip[time_idx, :, :], levels=self.levels,
                                         colors=self.color_arr, extend='max')
        else:
            # 1D data
            self.contour_handle = ax.tricontourf(self.lon, self.lat, self.precip[time_idx, :], levels=self.levels,
                                            colors=self.color_arr, extend='max')
        ax.set_xlabel('lon')
        ax.set_ylabel('lat')

        # datum text
        x_text = ax.get_xlim()[0] + 0.1 * (ax.get_xlim()[1] - ax.get_xlim()[0])
        y_text = ax.get_ylim()[0] + 0.1 * (ax.get_ylim()[1] - ax.get_ylim()[0])
        self.datum_handle = plt.text(x_text, y_text, self.time_values[time_idx].isoformat(), size=9)

        # product text
        x_text = ax.get_xlim()[0] + 0.1 * (ax.get_xlim()[1] - ax.get_xlim()[0])
        y_text = ax.get_ylim()[0] + 0.9 * (ax.get_ylim()[1] - ax.get_ylim()[0])
        self.product_handle = plt.text(x_text, y_text, data_source_names(self.data_source[time_idx]), size=9)
        cbar_handle = plt.colorbar(self.contour_handle, ax=ax, ticks=self.levels, shrink=0.9)
        cbar_handle.ax.set_title('mm/h', size=9)
        # fig.show()

    def plotting_seconds(self, time_idx: int):
        """
        Make the further contour plots of x, y, z variables with the color standards by the German Weather Service
        (DWD). Method plotting_first must run beforehand.

        :param time_idx: number of time step index at which the plot is built
        :type time_idx: int
        """
        print(f'time step {time_idx:03d}')

        for coll in self.contour_handle.collections:
            plt.gca().collections.remove(coll)
        self.contour_handle = plt.contourf(self.lon, self.lat, self.precip[time_idx], levels=self.levels,
                                           colors=self.color_arr, extend='max')
        self.datum_handle.set_text(self.time_values[time_idx].isoformat())
        self.product_handle.set_text(data_source_names(self.data_source[time_idx]))
        plt.draw()


if __name__ == '__main__':
    wd = WeatherData('../data/precipitation_data_example_202311262000.nc')
    # wd.plot_single(dir_out='../data', time_idx=list(range(3)))
    wd.animate_plot(dir_out='../data', time_idx=list(range(50)))
