#   @author: Michael Wagner
#   @organization: TU Dresden
#   @contact: michael.wagner@tu-dresden.de
"""
The visualisation package provides methods to visualise the generated precipitation that was derived via
met_entities.WeatherData class. Single pictures and an animation over a time index are possible.

The helper_module comprises the dictionary of data source indexes and the according names as used in
met_entities.WeatherData class. Second, it contains a function to calculate the appropriate figure size with the best
possible kilometer-aware isotropy.

The plotting is done in module plot_weather_data. It offers a WeatherData class to read in netcdf content from
met_entities.WeatherData.export_netcdf generated precipitation data. Further, the methods plot_single and animate_plot
plot/animate the data with the color standard from the German Weather Service (DWD).
"""